package ru.kai.homework;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMyContacts extends Fragment {


    public FragmentMyContacts() {
        // Required empty public constructor
    }

    public static FragmentMyContacts newInstance(Bundle bundle) {
        FragmentMyContacts currentFragment = new FragmentMyContacts();
        Bundle args = new Bundle();
        args.putBundle("gettedArgs", bundle);
        currentFragment.setArguments(args);
        return currentFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_contacts, container, false);
    }

}
