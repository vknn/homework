package ru.kai.homework;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import ru.kai.homework.Models.UserModel;

/**
 * Created by akabanov on 11.10.2017.
 */

public interface IRestAPIUsers {
    @GET("users")
    Call<List<UserModel>> loadUsers();
}
