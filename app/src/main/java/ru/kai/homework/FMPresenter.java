package ru.kai.homework;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.kai.homework.Models.CommentModel;
import ru.kai.homework.Models.ImageModel;
import ru.kai.homework.Models.PostModel;
import ru.kai.homework.Models.TodoModel;
import ru.kai.homework.Models.UserModel;

/**
 * Created by akabanov on 11.10.2017.
 */

public class FMPresenter implements IFMPresenter{
    FragmentMain fmView;
    List<UserModel> userList;

    Retrofit retrofit;
    //IRestAPIUsers restAPIUsers;

    String TAG = "FMPresenter";

    public FMPresenter(){
        userList = new ArrayList<>();
    }

    public void setView(@NonNull FragmentMain fmView){
        this.fmView = fmView;
    }

    @Override
    public void destroy() {
        this.fmView = null;
    }

    public  void loadUsers(){

        IRestAPIUsers restAPIUsers = retrofit.create(IRestAPIUsers.class);
        Call<List<UserModel>> call = restAPIUsers.loadUsers();

        // запускаем
        try {
            downloadUsers(call);
        } catch (IOException e) {
            e.printStackTrace();
            this.fmView.setServiceMessage(e.getMessage());
        }

    }

    public void loadImage(){
        IRestAPIImage restAPI = retrofit.create(IRestAPIImage.class);
        Call<ImageModel> call = restAPI.loadData("photos", 3);
        try {
            downloadPhoto(call);
        } catch (IOException e) {
            e.printStackTrace();
            this.fmView.setServiceMessage(e.getMessage());
        }

    }

    public void loadTodo(){
        IRestAPITodo restAPI = retrofit.create(IRestAPITodo.class);
        final Random random = new Random();
        int num = random.nextInt(199);
        Call<TodoModel> call = restAPI.loadData("todos", num);
        try {
            downloadTodo(call);
        } catch (IOException e) {
            e.printStackTrace();
            this.fmView.setServiceMessage(e.getMessage());
        }

    }

    public void loadPost(String strNum){
        if (strNum.isEmpty()){
            this.fmView.setServiceMessage(fmView.getString(R.string.enter_post_id_hint));
            return;
        }
        int num = Integer.parseInt(strNum);
        if (num > 100){
            this.fmView.setServiceMessage(fmView.getString(R.string.fail_post_id));
            return;
        }

        if (!initializeComponents())return;

        IRestAPIPost restAPI = retrofit.create(IRestAPIPost.class);
        Call<PostModel> call = restAPI.loadData("posts", num);
        try {
            downloadPost(call);
        } catch (IOException e) {
            e.printStackTrace();
            this.fmView.setServiceMessage(e.getMessage());
        }

    }

    public void loadComment(String strNum){
        if (strNum.isEmpty()){
            this.fmView.setServiceMessage(fmView.getString(R.string.enter_comment_hint));
            return;
        }
        int num = Integer.parseInt(strNum);
        if (num > 500){
            this.fmView.setServiceMessage(fmView.getString(R.string.fail_comment_id));
            return;
        }

        if (!initializeComponents())return;

        IRestAPIComment restAPI = retrofit.create(IRestAPIComment.class);
        Call<CommentModel> call = restAPI.loadData("comments", num);
        try {
            downloadComment(call);
        } catch (IOException e) {
            e.printStackTrace();
            this.fmView.setServiceMessage(e.getMessage());
        }

    }

    public boolean initializeComponents(){
        this.fmView.showLoading();

        retrofit = initializeRetrofit();
        if (retrofit == null)return false;
        if(!initializeInternet())return false;
        return true;
    }

    private Retrofit initializeRetrofit(){
        Retrofit rf = null;
        try {
            rf = new Retrofit.Builder()
                    .baseUrl("https://jsonplaceholder.typicode.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        } catch (Exception e) {
            this.fmView.hideLoading();
            this.fmView.setServiceMessage("no retrofit: " + e.getMessage());
            Log.i(TAG, "no retrofit: " + e.getMessage());
        }

        return rf;
    }

    private Boolean initializeInternet() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) fmView.getActivity().getSystemService(fmView.getContext().CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = connectivityManager.getActiveNetworkInfo();
        if (networkinfo != null && networkinfo.isConnected()) {
            return true;
        } else {
            this.fmView.setServiceMessage(fmView.getResources().getString(R.string.check_internet));
            return false;
        }
    }

    private void downloadUsers(Call<List<UserModel>> call) throws IOException {
        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        userList.clear();
                        UserModel curUserModel = null;
                        for (int i = 0; i < 5; i++) {     // response.body().size()
                            curUserModel = response.body().get(i);
                            userList.add(curUserModel);
                        }
                        fmView.renderUserList(userList);
                    }
                } else {
                    System.out.println("onResponse error: " + response.code());
                    //fmView.setServiceMessage("onResponse error: " + response.code());
                }
                fmView.hideLoading();
            }
            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {
                System.out.println("onFailure " + t);
                //fmView.setServiceMessage("onFailure " + t.getMessage());
                fmView.hideLoading();
            }
        });
    }

    private void downloadPhoto(Call<ImageModel> call) throws IOException {
        call.enqueue(new Callback<ImageModel>() {
            @Override
            public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        ImageModel curImageModel = response.body();
                        String uri = curImageModel.getThumbnailUrl();
                        fmView.loadImage(uri);
                    }
                } else {
                    System.out.println("onResponse error: " + response.code());
                    //fmView.setServiceMessage("onResponse error: " + response.code());
                }
                //fmView.hideLoading();
            }
            @Override
            public void onFailure(Call<ImageModel> call, Throwable t) {
                System.out.println("onFailure " + t);
                //fmView.setServiceMessage("onFailure " + t.getMessage());
                //fmView.hideLoading();
            }
        });
    }

    private void downloadTodo(Call<TodoModel> call) throws IOException {
        call.enqueue(new Callback<TodoModel>() {
            @Override
            public void onResponse(Call<TodoModel> call, Response<TodoModel> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        TodoModel curTodoModel = response.body();
                        String todoTitle = curTodoModel.getTitle();
                        fmView.renderTodo(todoTitle);
                    }
                } else {
                    System.out.println("onResponse error: " + response.code());
                    //fmView.setServiceMessage("onResponse error: " + response.code());
                }
                //fmView.hideLoading();
            }
            @Override
            public void onFailure(Call<TodoModel> call, Throwable t) {
                System.out.println("onFailure " + t);
                //fmView.setServiceMessage("onFailure " + t.getMessage());
                //fmView.hideLoading();
            }
        });
    }

    private void downloadPost(Call<PostModel> call) throws IOException {
        call.enqueue(new Callback<PostModel>() {
            @Override
            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        PostModel curPostModel = response.body();
                        String postBody = curPostModel.getBody();
                        fmView.renderPost(postBody);
                    }
                } else {
                    System.out.println("onResponse error: " + response.code());
                    //fmView.setServiceMessage("onResponse error: " + response.code());
                }
                //fmView.hideLoading();
            }
            @Override
            public void onFailure(Call<PostModel> call, Throwable t) {
                System.out.println("onFailure " + t);
                //fmView.setServiceMessage("onFailure " + t.getMessage());
                //fmView.hideLoading();
            }
        });
    }

    private void downloadComment(Call<CommentModel> call) throws IOException {
        call.enqueue(new Callback<CommentModel>() {
            @Override
            public void onResponse(Call<CommentModel> call, Response<CommentModel> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        CommentModel curCommentModel = response.body();
                        String CommentBody = curCommentModel.getBody();
                        fmView.renderComment(CommentBody);
                    }
                } else {
                    System.out.println("onResponse error: " + response.code());
                    //fmView.setServiceMessage("onResponse error: " + response.code());
                }
                //fmView.hideLoading();
            }
            @Override
            public void onFailure(Call<CommentModel> call, Throwable t) {
                System.out.println("onFailure " + t);
                //fmView.setServiceMessage("onFailure " + t.getMessage());
                //fmView.hideLoading();
            }
        });
    }

}
