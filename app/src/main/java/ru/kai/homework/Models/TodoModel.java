package ru.kai.homework.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents a user in the presentation layer.
 * сеттеры не используем, так как по условиям задачи - только вывод списка
 */
public class TodoModel {

  @SerializedName("id")
  @Expose
  private final int todoId;

  public TodoModel(int userId) {
    this.todoId = userId;
  }

  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("completed")
  @Expose
  private Boolean completed;

  public int getTodoId() {
    return todoId;
  }

  public String getTitle() {
    return title;
  }

  public Boolean getCompleted() {
    return completed;
  }


}
