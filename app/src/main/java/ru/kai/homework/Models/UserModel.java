package ru.kai.homework.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents a user in the presentation layer.
 * сеттеры не используем, так как по условиям задачи - только вывод списка
 */
public class UserModel {

  @SerializedName("id")
  @Expose
  private final int userId;

  public UserModel(int userId) {
    this.userId = userId;
  }

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("username")
  @Expose
  private String userName;
  @SerializedName("email")
  @Expose
  private String email;

  public int getUserId() {
    return userId;
  }

  public String getName() {
    return name;
  }

  public String getUserName() {
    return userName;
  }

  public String getEmail() {
    return email;
  }

}
