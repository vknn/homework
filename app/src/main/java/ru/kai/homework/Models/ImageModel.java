package ru.kai.homework.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents a user in the presentation layer.
 * сеттеры не используем, так как по условиям задачи - только вывод списка
 */
public class ImageModel {

  @SerializedName("id")
  @Expose
  private final int imageId;

  public ImageModel(int userId) {
    this.imageId = userId;
  }

  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("url")
  @Expose
  private String url;
  @SerializedName("thumbnailUrl")
  @Expose
  private String thumbnailUrl;

  public int getImageId() {
    return imageId;
  }

  public String getTitle() {
    return title;
  }

  public String getUrl() {
    return url;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

}
