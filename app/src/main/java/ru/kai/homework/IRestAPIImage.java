package ru.kai.homework;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.kai.homework.Models.ImageModel;
import ru.kai.homework.Models.UserModel;

/**
 * Created by akabanov on 11.10.2017.
 */

public interface IRestAPIImage {
    @GET("{param}/{num}")
    Call<ImageModel> loadData(@Path("param") String param, @Path("num") int num);
}
